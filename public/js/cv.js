$(document).ready(function() {
  var employmentData;

  // Click handlers for change tables
  $('.cv-change-table .cv-button').click(function(evt) {
    if ($(this).hasClass('cv-button-active')) {
      return;
    }
    $(this).closest('.cv-change-table').find('.cv-button').removeClass('cv-button-active').addClass('cv-button-inactive');
    $(this).removeClass('cv-button-inactive').addClass('cv-button-active');
    $(this).closest('.cv-change-table').find('.cv-content-item').addClass('cv-content-inactive')
        .filter('#' + $(this).attr('id') + 'Content').removeClass('cv-content-inactive');
  });

  // Construct Experience table from employment JSON data
  d3.json('json/employment.json', function(data) {
    var scale, rootDiv, svg, countriesG, countryNodes, jobNodes,
        fHeight = 70,
        separatorHeight = 1,
        yearGap = 24,
        countries = data.countries,
        jobs = data.jobs,
        width = 940,
        margins = {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        };

    employmentData = data;
    countries.sort(function(a, b) {
      return getDate(b.startDate).getTime() - getDate(a.startDate).getTime();
    });

    countries.reverse();
    jobs.reverse();

    scale = d3.time.scale()
        .domain(
            [d3.min(countries, function(c) {
              return getDate(c.startDate);
            }), d3.max(countries, function(c) {
              return getDate(c.endDate);
            })]
        )
        .rangeRound([width - margins.right, 0 + margins.left]);

    rootDiv = d3.select('#employment');
    svg = rootDiv.append('svg');
    countriesG = svg.append('g');

    svg.classed('cv-svg', true);
    countryNodes = countriesG.selectAll('g')
        .data(countries)
          .enter()
            .append('g')
    countryNodes.attr('transform', function(d) {
      // load svgs here
      var thisG = this;
      d3.xml('flags/' + d.code + 'Flag.svg', function(xml) {
        var _d = d;
        var fWidth = scale(getDate(_d.startDate)) - scale(getDate(_d.endDate));
        var flag = thisG.appendChild(xml.documentElement);
        d3.select(flag)
            .attr('width', fWidth)
            .attr('height', fHeight)
              .attr('preserveAspectRatio', 'none'/*'xMidYMid slice'*/)
        if (d.code == 'AUS') {
          d3.select(flag).attr('preserveAspectRatio', 'xMidYMid slice');
        }
      });
      return 'translate(' + scale(getDate(d.endDate)) + ',' + separatorHeight + ')';
    });
    countryNodes.attr('opacity', 0.8);

    var yearAxis = d3.svg.axis()
        .scale(scale)
        .orient('bottom')
        .ticks(5);
    var tick = svg.append('g')
        .attr('transform', 'translate(0,' + function(){return fHeight + separatorHeight}() + ')')
        .attr('class', 'axis')
        .call(yearAxis);
    tick.selectAll('line').attr('x1', 0).attr('y1', yearGap).attr('x2', 0).attr('y2', 0)
        .style('stroke-width', separatorHeight).style('stroke', 'black');
    tick.selectAll('text').attr('x', -8).style('text-anchor', 'end');
    tick.selectAll('.domain').remove();

    // Dividers
    svg.append('g').append('rect').attr('x', 0).attr('y', fHeight + separatorHeight + yearGap).attr('width', width).attr('height', separatorHeight);
    svg.append('g').append('rect').attr('x', 0).attr('y', fHeight + separatorHeight).attr('width', width).attr('height', separatorHeight);
    svg.append('g').append('rect').attr('x', 0).attr('y', 0).attr('width', width).attr('height', separatorHeight);
    svg.append('g').append('rect').attr('x', 0).attr('y', 0).attr('width', separatorHeight).attr('height', fHeight + (separatorHeight * 3) + yearGap);
    svg.append('g').append('rect').attr('x', width -1).attr('y', 0).attr('width', separatorHeight).attr('height', fHeight + (separatorHeight * 3) + yearGap);

    svg.attr('height', fHeight + (separatorHeight*3) + yearGap).attr('width', 940);

    jobNodes = rootDiv.append('div').attr('class', 'cv-job-row').selectAll('.cv-job-div')
        .data(jobs).enter().append('div').attr('class', 'cv-job-div');

    jobNodes.style('max-width', function(d, i) {
      var div = d3.select(this);
      div.append('p').attr('class', 'cv-period').text(formatDate(d.startDate) + ' - ' + formatDate(d.endDate));
      var par = div.append('p');
      par.append('span').attr('class', 'cv-job-title').text(d.title + ' ');
      par.append('span').attr('class', 'cv-job-firm').text(d.firm);
      if (i == 0) {
        $(this).css('border-left', '1px solid black');
      }
      $.each(d.description, function(index, desc) {
        div.append('p').attr('class', 'job-description').text(desc);
      });
      return function() {return scale(getDate(d.startDate)) - scale(getDate(d.endDate))}() + 'px';
    });
  });

  function getDate(str) {
    return str == 'current' ?
        new Date() :
        new Date(str);
  }

  function formatDate(str) {
    var d,
        options = {
          year: 'numeric', month: 'long'
        };
    if (str == 'current') {
      return 'present';
    }
    d = new Date(str);
    return d.toLocaleDateString('en-US', options);
  }
});